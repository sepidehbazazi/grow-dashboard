FROM python:3.6

RUN mkdir /app
COPY ./requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

COPY . /app
WORKDIR /app

VOLUME /app


CMD ["python", "grow_dashboard.py"]