import dash
import dash_core_components as dcc
import dash_html_components as html
# import dash_table_experiments as dt
# import plotly.graph_objs as go
import pandas as pd
import numpy as np
# from flask import send_file
# import io
# import flask

# import src.constants
# import src.helper_functions
# from src.data_sources import GDeltData, CreditData



def main():
    """
    Configures and turns on the main dashboard application
    :return:
    """
    vertical = False
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    regions = pd.read_csv('data/regions.csv', encoding = 'latin')
    europe = regions[regions['Region'] == 'Europe']
    imea = regions[regions['Region'] == "IMEA"]
    amer = regions[regions['Region'] == "Americas"]
    apr = regions[regions['Region'] == "APR"]

    # When these .csv files were compiled from the raw data:
    # 1. the distinction between Mainland China and Hong Kong were made manually in the docs
    # 2. Republic of Korea was used instead of Korea, Republic of.

    air_travel_emissions = pd.read_csv('data/air_travel_emissions.csv', encoding = 'latin')

    # When these .csv files were compiled from the raw data:
    # 1. there was no headcount data for 2017 and 2018, so the 2019 data was used in these years
    # 2. Republic of Korea was used instead of Korea, Republic of.
    # 3. United States was changed to United States of America
    # 4. Only Employee Types Regular, Temporary and Intern was considered.

    head_counts = pd.read_csv('data/20210621_headcounts.csv', encoding = 'latin')
    head_counts_by_country = head_counts.groupby(['Year', 'Location Address - Country'])['Location'].count().reset_index()

    head_counts_by_country = head_counts_by_country.rename(columns={"Location Address - Country": "Country",
                                                                    "Location": "Total head count"})

    # office_emissions = pd.read_csv('data/office_emissions.csv', encoding = 'latin')
    # office_emissions_by_country = office_emissions.groupby(['Country'])['Total Mwh', 'CO2e (Metric Tonnes)'].sum().reset_index()

    office_emissions = pd.read_csv('data/office_scope1_and_2_emissions.csv', encoding='latin')
    office_emissions_by_country = office_emissions.groupby(['Country'])['COMBINED Total GHG Emissions (Metric Tonnes CO2e)'].sum().reset_index()

    office_size = pd.read_csv('data/office_size.csv', encoding = 'latin')
    office_size['Square meters'] = office_size['Square feet'] * 0.09290304


    travelling_head_counts = head_counts[(head_counts["OW Staff Type"].isin(["Consultant - Consultant (Staff Type)",
                                                                            "Partner - Partner (Staff Type)"])) | (head_counts["Location Address - Country"]=="Ireland")]

    travelling_head_counts_by_country = travelling_head_counts.groupby(['Year', 'Location Address - Country'])['Location'].count().reset_index()
    travelling_head_counts_by_country = travelling_head_counts_by_country.rename(columns={"Location Address - Country": "Country",
                                                                    "Location": "Total head count"})

    office_size_by_country = office_size.groupby(['Country'])['Square feet', 'Square meters'].sum()

    rail_travel_emissions = pd.read_csv('data/rail_travel_emissions.csv', encoding='latin')
    rail_travel_emissions_by_country = rail_travel_emissions.groupby(['Year', 'Traveler Country'])['Emissions_kgCO2e'].sum().reset_index()
    rail_travel_emissions_by_country = rail_travel_emissions_by_country.rename(columns={"Traveler Country": "Country"})
    rail_travel_emissions_by_country = rail_travel_emissions_by_country[
        (rail_travel_emissions_by_country["Country"] != 'Not Provided')]


    hotel_stay_emissions = pd.read_csv('data/hotel_stay_emissions.csv', encoding='latin')
    hotel_stay_emissions_by_country = hotel_stay_emissions.groupby(['Year', 'Traveler Country'])['total_kgCO2e_emissions'].sum().reset_index()
    hotel_stay_emissions_by_country = hotel_stay_emissions_by_country.rename(columns={"Traveler Country": "Country"})

    table = pd.pivot_table(air_travel_emissions, values='Defra(Kg CO2e)', index=['Country'],
                           columns=['Year'], aggfunc=np.sum).reset_index().rename_axis('index',axis=1)
    table["YoY_Change2019"] = (table[2019] - table[2018]) / (table[2018]) * 100
    table["YoY_Change2018"] = (table[2018] - table[2017]) / (table[2017]) * 100



    df_all = travelling_head_counts_by_country.merge(air_travel_emissions, how='left', on=['Year', 'Country'])
    df_all["Air"] = df_all["Defra(Kg CO2e)"] / df_all["Total head count"]

    df_all = df_all.merge(rail_travel_emissions_by_country, how='left', on=['Year', 'Country'])
    df_all["Rail"] = df_all["Emissions_kgCO2e"] / df_all["Total head count"]

    df_all = df_all.merge(hotel_stay_emissions_by_country, how='left', on=['Year', 'Country'])
    df_all["Hotel"] = df_all["total_kgCO2e_emissions"] / df_all["Total head count"]
    df_all = df_all.drop(["Defra(Kg CO2e)", "Emissions_kgCO2e", "total_kgCO2e_emissions", "Total head count"], axis=1)
    melted = df_all.melt(id_vars=["Year", "Country"], var_name="Type")
    pivoted = melted.pivot(index=["Country", "Type"], columns="Year", values="value")

    pivoted["2018-2019 % Change"] = ((pivoted[2019] - pivoted[2018]) / (pivoted[2018]) * 100).round(1)
    pivoted["2019-2020 % Change"] = ((pivoted[2020] - pivoted[2019]) / (pivoted[2019]) * 100).round(1   )
    # pivoted.to_csv('output/emissions_country_type   .csv')


    app.title = 'OW Carbon Offsetting Project Dashboard'

    app.layout = html.Div([
        html.Div(
            className="app-logo",
            children=[
                html.Img(src='assets/ow-logo-new2.png')
            ],
            style={
                'textAlign': 'left',
            }
        ),
        html.Div(
            [
                html.H3('OW Carbon Offsetting Project Dashboard'),
            ]
        ),
        dcc.Tabs(id="tabs",  vertical=vertical, children=[
            dcc.Tab(label='Europe', children=[
                # EUROPE AIR TRAVEL EMISSIONS
                html.Div(
                    [
                        html.Div('Select Year'),
                        dcc.Dropdown(
                            id='europe-crossfilter-year-keyword',
                            options=[{'label': i, 'value': i} for i in air_travel_emissions.Year.unique()],
                            value=air_travel_emissions.Year[0]
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='europe-graph-air-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],

                ),
                html.Div(
                    [
                        html.Div('Select Country'),
                        dcc.Dropdown(
                            id='europe-air-crossfilter-country-keyword',
                            options=[{'label': i, 'value': i} for i in europe.Country],
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='europe-graph-air-emission-period'),
                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),

                # EUROPE OFFICE EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='europe-graph-office-emission'),
                            ],
                        ),
                    ],
                ),

                # EUROPE RAIL EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='europe-graph-rail-emission'),

                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Rail emissions are displayed for each traveller's country, but calculated based on their travel origin and destination"),
                    ],
                ),

                # EUROPE HOTEL STAY EMISSIONS
                html.Div(
                    [
                        html.Div(
                                [
                                    dcc.Graph(id='europe-graph-hotels-emission'),
                                ],
                                style={'padding': 25    }

                            ),

                        ],
                    ),
                html.Div(
                    [
                        html.Div(
                            "Hotel emissions are displayed for each traveller's country, but calculated based on their hotel location"),
                    ],

                ),
            ]),
            dcc.Tab(label='IMEA', children=[
                # IMEA AIR TRAVEL EMISSIONS
                html.Div(
                    [
                        html.Div('Select Year'),
                        dcc.Dropdown(
                            id='imea-crossfilter-year-keyword',
                            options=[{'label': i, 'value': i} for i in air_travel_emissions.Year.unique()],
                            value=air_travel_emissions.Year[0]
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='imea-graph-air-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],

                ),
                html.Div(
                    [
                        html.Div('Select Country'),
                        dcc.Dropdown(
                            id='imea-air-crossfilter-country-keyword',
                            options=[{'label': i, 'value': i} for i in imea.Country],
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='imea-graph-air-emission-period'),
                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),

                # IMEA OFFICE EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='imea-graph-office-emission'),
                            ],
                        ),
                    ],
                ),

                # IMEA RAIL EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='imea-graph-rail-emission'),

                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Rail emissions are displayed for each traveller's country, but calculated based on their travel origin and destination"),
                    ],
                ),

                # IMEA HOTEL STAY EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='imea-graph-hotels-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Hotel emissions are displayed for each traveller's country, but calculated based on their hotel location"),
                    ],

                ),
            ]),
            dcc.Tab(label='Americas', children=[
                # AMER AIR TRAVEL EMISSIONS
                html.Div(
                    [
                        html.Div('Select Year'),
                        dcc.Dropdown(
                            id='amer-crossfilter-year-keyword',
                            options=[{'label': i, 'value': i} for i in air_travel_emissions.Year.unique()],
                            value=air_travel_emissions.Year[0]
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='amer-graph-air-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],

                ),
                html.Div(
                    [
                        html.Div('Select Country'),
                        dcc.Dropdown(
                            id='amer-air-crossfilter-country-keyword',
                            options=[{'label': i, 'value': i} for i in amer.Country],
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='amer-graph-air-emission-period'),
                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),

                # AMER OFFICE EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='amer-graph-office-emission'),
                            ],
                        ),
                    ],
                ),

                # AMER RAIL EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='amer-graph-rail-emission'),

                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Rail emissions are displayed for each traveller's country, but calculated based on their travel origin and destination"),
                    ],
                ),

                # AMER HOTEL STAY EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='amer-graph-hotels-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Hotel emissions are displayed for each traveller's country, but calculated based on their hotel location"),
                    ],

                ),

            ]),
            dcc.Tab(label='APR', children=[
                # APR AIR TRAVEL EMISSIONS
                html.Div(
                    [
                        html.Div('Select Year'),
                        dcc.Dropdown(
                            id='apr-crossfilter-year-keyword',
                            options=[{'label': i, 'value': i} for i in air_travel_emissions.Year.unique()],
                            value=air_travel_emissions.Year[0]
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='apr-graph-air-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],

                ),
                html.Div(
                    [
                        html.Div('Select Country'),
                        dcc.Dropdown(
                            id='apr-air-crossfilter-country-keyword',
                            options=[{'label': i, 'value': i} for i in apr.Country],
                        )
                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='apr-graph-air-emission-period'),
                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),

                # APR OFFICE EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='apr-graph-office-emission'),
                            ],
                        ),
                    ],
                ),

                # APR RAIL EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='apr-graph-rail-emission'),

                            ],
                            style={'padding': 25    }
                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Rail emissions are displayed for each traveller's country, but calculated based on their travel origin and destination"),
                    ],
                ),

                # APR HOTEL STAY EMISSIONS
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Graph(id='apr-graph-hotels-emission'),
                            ],
                            style={'padding': 25    }

                        ),

                    ],
                ),
                html.Div(
                    [
                        html.Div(
                            "Hotel emissions are displayed for each traveller's country, but calculated based on their hotel location"),
                    ],

                ),

            ]),

        ])
    ])


    @app.callback(
        dash.dependencies.Output('europe-graph-air-emission', 'figure'),
        [dash.dependencies.Input('europe-crossfilter-year-keyword', 'value'),
        ])
    def update_air_emission_figure(year):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[(travelling_head_counts_by_country["Year"] == year)]
        all_regions_df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])
        all_regions_df["Defra(Kg CO2e) Per Head"] = all_regions_df["Defra(Kg CO2e)"] / all_regions_df["Total head count"]

        df = regions.merge(all_regions_df, how='left', on="Country")
        df = df[df['Region'] == 'Europe']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Air Travel - Defra(Kg CO2e) Per Head",
            yaxis=dict(
                range=[0, 20000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('europe-graph-air-emission-period', 'figure'),
        [dash.dependencies.Input('europe-air-crossfilter-country-keyword', 'value'),
         ])
    def update_air_emission_period_figure(country):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Country"] == country)]

        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Country"] == country)]

        df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])


        df["Defra(Kg CO2e) Per Head"] = df["Defra(Kg CO2e)"] / df["Total head count"]
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="lines+markers",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="scatter",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            xaxis=dict(
                tickmode='array',
                tickvals=[2017, 2018, 2019, 2020],
                ticktext=['2017', '2018', '2019', '2020']
            ),
            yaxis = dict(
                range=[0,40000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)

        return figure


    @app.callback(
        dash.dependencies.Output('europe-graph-office-emission', 'figure'),
        [dash.dependencies.Input('europe-crossfilter-year-keyword', 'value'),
         ])
    def update_office_emission_figure(year):
        """
        Update the figure with the office carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """
        df = regions.merge(head_counts_by_country, how='left', on='Country')
        df = df.merge(office_emissions_by_country, how='left', on='Country')
        df = df.merge(office_size_by_country, how='left', on='Country')

        df["CO2e (Metric Tonnes) per square meter"] = df["COMBINED Total GHG Emissions (Metric Tonnes CO2e)"] / df["Square meters"]

        df = df[df["Region"] == 'Europe']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Satellite Overview",

        )

        layout["title"] = "Office emisions - CO2e (metric tonnes) per square meter"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('europe-graph-rail-emission', 'figure'),
        [dash.dependencies.Input('europe-crossfilter-year-keyword', 'value'),

        ])
    def update_rail_emission_figure(year):
        """
        Update the figure with the rail travel carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        rail = rail_travel_emissions_by_country[(rail_travel_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]
        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(rail, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["Emissions_kgCO2e"] / df["Total head count"]
        df = df[df["Region"] == 'Europe']
        df = df.sort_values(by=['Country'])
        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 200]
            )
        )

        layout["title"] = "Rail - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('europe-graph-hotels-emission', 'figure'),
        [dash.dependencies.Input('europe-crossfilter-year-keyword', 'value'),

         ])
    def update_hotels_emission_figure(year):
        """
        Update the figure with the hotels stay carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        hotel_stay = hotel_stay_emissions_by_country[(hotel_stay_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]

        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(hotel_stay, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["total_kgCO2e_emissions"] / df["Total head count"]
        df = df[df["Region"] == 'Europe']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip"),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",

            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 2000],
            ),

        )

        layout["title"] = "Hotel Stay - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure


    @app.callback(
        dash.dependencies.Output('imea-graph-air-emission', 'figure'),
        [dash.dependencies.Input('imea-crossfilter-year-keyword', 'value'),
        ])
    def update_air_emission_figure(year):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[(travelling_head_counts_by_country["Year"] == year)]
        all_regions_df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])
        all_regions_df["Defra(Kg CO2e) Per Head"] = all_regions_df["Defra(Kg CO2e)"] / all_regions_df["Total head count"]

        df = regions.merge(all_regions_df, how='left', on="Country")
        df = df[df['Region'] == 'IMEA']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Air Travel - Defra(Kg CO2e) Per Head",
            yaxis=dict(
                range=[0, 20000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('imea-graph-air-emission-period', 'figure'),
        [dash.dependencies.Input('imea-air-crossfilter-country-keyword', 'value'),
         ])
    def update_air_emission_period_figure(country):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Country"] == country)]

        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Country"] == country)]

        df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])


        df["Defra(Kg CO2e) Per Head"] = df["Defra(Kg CO2e)"] / df["Total head count"]
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="lines+markers",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="scatter",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            xaxis=dict(
                tickmode='array',
                tickvals=[2017, 2018, 2019, 2020],
                ticktext=['2017', '2018', '2019', '2020']
            ),
            yaxis = dict(
                range=[0,40000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)

        return figure


    @app.callback(
        dash.dependencies.Output('imea-graph-office-emission', 'figure'),
        [dash.dependencies.Input('imea-crossfilter-year-keyword', 'value'),
         ])
    def update_office_emission_figure(year):
        """
        Update the figure with the office carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """
        df = regions.merge(head_counts_by_country, how='left', on='Country')
        df = df.merge(office_emissions_by_country, how='left', on='Country')
        df = df.merge(office_size_by_country, how='left', on='Country')

        df["CO2e (Metric Tonnes) per square meter"] = df["COMBINED Total GHG Emissions (Metric Tonnes CO2e)"] / df["Square meters"]

        df = df[df["Region"] == 'IMEA']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Satellite Overview",

        )

        layout["title"] = "Office emisions - CO2e (metric tonnes) per square meter"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('imea-graph-rail-emission', 'figure'),
        [dash.dependencies.Input('imea-crossfilter-year-keyword', 'value'),

        ])
    def update_rail_emission_figure(year):
        """
        Update the figure with the rail travel carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        rail = rail_travel_emissions_by_country[(rail_travel_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]
        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(rail, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["Emissions_kgCO2e"] / df["Total head count"]
        df = df[df["Region"] == 'IMEA']
        df = df.sort_values(by=['Country'])
        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 200]
            )
        )

        layout["title"] = "Rail - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('imea-graph-hotels-emission', 'figure'),
        [dash.dependencies.Input('imea-crossfilter-year-keyword', 'value'),

         ])
    def update_hotels_emission_figure(year):
        """
        Update the figure with the hotels stay carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        hotel_stay = hotel_stay_emissions_by_country[(hotel_stay_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]

        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(hotel_stay, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["total_kgCO2e_emissions"] / df["Total head count"]
        df = df[df["Region"] == 'IMEA']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip"),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",

            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 2000],
            ),

        )

        layout["title"] = "Hotel Stay - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure


    @app.callback(
        dash.dependencies.Output('amer-graph-air-emission', 'figure'),
        [dash.dependencies.Input('amer-crossfilter-year-keyword', 'value'),
        ])
    def update_air_emission_figure(year):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[(travelling_head_counts_by_country["Year"] == year)]
        all_regions_df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])
        all_regions_df["Defra(Kg CO2e) Per Head"] = all_regions_df["Defra(Kg CO2e)"] / all_regions_df["Total head count"]

        df = regions.merge(all_regions_df, how='left', on="Country")
        df = df[df['Region'] == 'Americas']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Air Travel - Defra(Kg CO2e) Per Head",
            yaxis=dict(
                range=[0, 20000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('amer-graph-air-emission-period', 'figure'),
        [dash.dependencies.Input('amer-air-crossfilter-country-keyword', 'value'),
         ])
    def update_air_emission_period_figure(country):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Country"] == country)]

        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Country"] == country)]

        df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])


        df["Defra(Kg CO2e) Per Head"] = df["Defra(Kg CO2e)"] / df["Total head count"]
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="lines+markers",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="scatter",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            xaxis=dict(
                tickmode='array',
                tickvals=[2017, 2018, 2019, 2020],
                ticktext=['2017', '2018', '2019', '2020']
            ),
            yaxis = dict(
                range=[0,40000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)

        return figure


    @app.callback(
        dash.dependencies.Output('amer-graph-office-emission', 'figure'),
        [dash.dependencies.Input('amer-crossfilter-year-keyword', 'value'),
         ])
    def update_office_emission_figure(year):
        """
        Update the figure with the office carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """
        df = regions.merge(head_counts_by_country, how='left', on='Country')
        df = df.merge(office_emissions_by_country, how='left', on='Country')
        df = df.merge(office_size_by_country, how='left', on='Country')

        df["CO2e (Metric Tonnes) per square meter"] = df["COMBINED Total GHG Emissions (Metric Tonnes CO2e)"] / df["Square meters"]

        df = df[df["Region"] == 'Americas']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Satellite Overview",

        )

        layout["title"] = "Office emisions - CO2e (metric tonnes) per square meter"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('amer-graph-rail-emission', 'figure'),
        [dash.dependencies.Input('amer-crossfilter-year-keyword', 'value'),

        ])
    def update_rail_emission_figure(year):
        """
        Update the figure with the rail travel carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        rail = rail_travel_emissions_by_country[(rail_travel_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]
        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(rail, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["Emissions_kgCO2e"] / df["Total head count"]
        df = df[df["Region"] == 'Americas']
        df = df.sort_values(by=['Country'])
        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 200]
            )
        )

        layout["title"] = "Rail - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('amer-graph-hotels-emission', 'figure'),
        [dash.dependencies.Input('amer-crossfilter-year-keyword', 'value'),

         ])
    def update_hotels_emission_figure(year):
        """
        Update the figure with the hotels stay carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        hotel_stay = hotel_stay_emissions_by_country[(hotel_stay_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]

        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(hotel_stay, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["total_kgCO2e_emissions"] / df["Total head count"]
        df = df[df["Region"] == 'Americas']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip"),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",

            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 2000],
            ),

        )

        layout["title"] = "Hotel Stay - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure


    @app.callback(
        dash.dependencies.Output('apr-graph-air-emission', 'figure'),
        [dash.dependencies.Input('apr-crossfilter-year-keyword', 'value'),
        ])
    def update_air_emission_figure(year):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[(travelling_head_counts_by_country["Year"] == year)]
        all_regions_df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])
        all_regions_df["Defra(Kg CO2e) Per Head"] = all_regions_df["Defra(Kg CO2e)"] / all_regions_df["Total head count"]

        df = regions.merge(all_regions_df, how='left', on="Country")
        df = df[df['Region'] == 'APR']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Air Travel - Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Air Travel - Defra(Kg CO2e) Per Head",
            yaxis=dict(
                range=[0, 20000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('apr-graph-air-emission-period', 'figure'),
        [dash.dependencies.Input('apr-air-crossfilter-country-keyword', 'value'),
         ])
    def update_air_emission_period_figure(country):
        """
        Update the figure with the air travel carbon emission for the selected
         year(s) and country in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        air = air_travel_emissions[(air_travel_emissions["Country"] == country)]

        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Country"] == country)]

        df = travelling_head_counts_by_country_year.merge(air, how='left', on=['Year', 'Country'])


        df["Defra(Kg CO2e) Per Head"] = df["Defra(Kg CO2e)"] / df["Total head count"]
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="lines+markers",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="scatter",
                x=df["Year"],
                y=df["Defra(Kg CO2e) Per Head"],
                name="Defra(Kg CO2e) Per Head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            xaxis=dict(
                tickmode='array',
                tickvals=[2017, 2018, 2019, 2020],
                ticktext=['2017', '2018', '2019', '2020']
            ),
            yaxis = dict(
                range=[0,40000]
            )
        )

        layout["title"] = "Air Travel - Defra(Kg CO2e) per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)

        return figure


    @app.callback(
        dash.dependencies.Output('apr-graph-office-emission', 'figure'),
        [dash.dependencies.Input('apr-crossfilter-year-keyword', 'value'),
         ])
    def update_office_emission_figure(year):
        """
        Update the figure with the office carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """
        df = regions.merge(head_counts_by_country, how='left', on='Country')
        df = df.merge(office_emissions_by_country, how='left', on='Country')
        df = df.merge(office_size_by_country, how='left', on='Country')

        df["CO2e (Metric Tonnes) per square meter"] = df["COMBINED Total GHG Emissions (Metric Tonnes CO2e)"] / df["Square meters"]

        df = df[df["Region"] == 'APR']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["CO2e (Metric Tonnes) per square meter"],
                name="CO2e (Metric Tonnes) per square meter",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="Satellite Overview",

        )

        layout["title"] = "Office emisions - CO2e (metric tonnes) per square meter"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('apr-graph-rail-emission', 'figure'),
        [dash.dependencies.Input('apr-crossfilter-year-keyword', 'value'),

        ])
    def update_rail_emission_figure(year):
        """
        Update the figure with the rail travel carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        rail = rail_travel_emissions_by_country[(rail_travel_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]
        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(rail, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["Emissions_kgCO2e"] / df["Total head count"]
        df = df[df["Region"] == 'APR']
        df = df.sort_values(by=['Country'])
        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip",
            ),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=30, b=80, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 200]
            )
        )

        layout["title"] = "Rail - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure

    @app.callback(
        dash.dependencies.Output('apr-graph-hotels-emission', 'figure'),
        [dash.dependencies.Input('apr-crossfilter-year-keyword', 'value'),

         ])
    def update_hotels_emission_figure(year):
        """
        Update the figure with the hotels stay carbon emission for the selected
         year(s) in the selected date range
        :param year: Year(s) associated with the carbon emission
        :return:
        """

        hotel_stay = hotel_stay_emissions_by_country[(hotel_stay_emissions_by_country["Year"] == year)]
        travelling_head_counts_by_country_year = travelling_head_counts_by_country[
            (travelling_head_counts_by_country["Year"] == year)]

        region_df = regions.merge(travelling_head_counts_by_country_year, how='left', on='Country')
        df = region_df.merge(hotel_stay, how='left', on='Country')
        df["Emissions kgCO2e per head"] = df["total_kgCO2e_emissions"] / df["Total head count"]
        df = df[df["Region"] == 'APR']
        df = df.sort_values(by=['Country'])

        data = [
            dict(
                type="scatter",
                mode="markers",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",
                opacity=0,
                hoverinfo="skip"),
            dict(
                type="bar",
                x=df["Country"],
                y=df["Emissions kgCO2e per head"],
                name="Emissions kgCO2e per head",

            ),
        ]

        layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(l=30, r=80, b=110, t=40),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation="h"),
            title="",
            yaxis=dict(
                range=[0, 2000],
            ),

        )

        layout["title"] = "Hotel Stay - Emissions kgCO2e per head (traveller)"
        layout["showlegend"] = False
        layout["autosize"] = True

        figure = dict(data=data, layout=layout)
        return figure



    app.run_server(debug=True, use_reloader=True, host="0.0.0.0")

if __name__ == '__main__':
    main()
