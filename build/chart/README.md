## Configuration

The following table lists the configurable parameters of the webtool chart and its default values.

> Please note this file is intended to be used within the context of the Jenkinsfile.deploy job,
> which will provide the values for these parameters based on the job arguments and environment variables

| Parameter                   | Description                                                                    | Default                               |
|-----------------------------|--------------------------------------------------------------------------------|---------------------------------------|
| `environment`               | Identifier of the environment being deployed.                                  | `latestcommit`                        |
| `webtool.image`             | Webtool image name plus tag.                                                   | ``                                    |
| `webtool.versionTag`        | Tag identifying the webtool version being deployed                             | `latest`                              |
| `webtool.commit`            | Git commitId corresponding to the webtool images being deployed                | ``                                    |
| `webtool.replicas`          | Number of replica containers.                                                  | `1`                                   |
| `webtool.secretsName`       | The name of the Rancher secret containing the mongo connections.               | `labskit-secrets`             |
| `webtool.publicDns`         | Whether to create a public DNS endpoint for the app.                           | `false`                               |
| `webtool.publicDnsTarget`   | DNS endpoint for public network.                                               | Use appropriate environment variable  |
| `webtool.privateDnsTarget`  | DNS endpoint for private network.                                              | Use appropriate environment variable  |
