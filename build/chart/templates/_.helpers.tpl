{{/*
Set webtool domain
*/}}
{{- define "webtool.domain" -}}
{{- printf "webtool.%s.%s.%s.owlabs.io" .Release.Namespace .Values.clusterName .Values.regionName -}}
{{- end -}}

{{/*
If you want to use a custom domain name for production that doesnt follow the usual DNS convention. See https://owlabs.atlassian.net/wiki/spaces/CHIPS/pages/1121616057/DNS+and+SSL#Custom-owlabs.io-domain-names
You could add a conditional to the helper above as:
{{- define "webtool.domain" -}}
{{- if (eq .Values.environment "production") -}}
{{- printf "some-custom-domain.owlabs.io" -}}
{{- else -}}
{{- printf "webtool.%s.%s.%s.owlabs.io" .Release.Namespace .Values.clusterName .Values.regionName -}}
{{- end -}}
{{- end -}}
*/}}

{{/*
Set webtool SSL secret name based on the ingress name
NOTE: it must follow a very explicit convention based on the domain name
  so the certbot can generae the secret and store it.
  This helper ensures the secret name always follows that convention. See https://owlabs.atlassian.net/wiki/spaces/CHIPS/pages/1121616057/DNS+and+SSL#SSL%2FTLS-CertBot
*/}}
{{- define "webtool.sslsecret" -}}
{{- splitList "." ( include "webtool.domain" . ) | rest | join "-" -}}
{{- end -}}

{{/*
Returns a set of labels applied to each webtool resource.
*/}}
{{- define "webtool.labels" -}}
app.kubernetes.io/name: webtool
app.kubernetes.io/version: "{{ .Values.webtool.versionTag }}"
app.kubernetes.io/version-hash: "{{ .Values.webtool.commit }}"
app.kubernetes.io/instance: "{{ .Values.environment }}"
app.kubernetes.io/part-of: "{{ .Values.projectName }}"
app.kubernetes.io/managed-by: "{{ .Release.Service }}"
helm.sh/chart: "{{ .Chart.Name }}-{{.Chart.Version | replace "+" "_" }}"
{{- end -}}

{{/*
Returns a set of matchLabels applied to webtool resources like Deployments that need to match pods.
*/}}
{{- define "webtool.matchLabels" -}}
app.kubernetes.io/name: webtool
app.kubernetes.io/instance: "{{ .Values.environment }}"
{{- end -}}

{{/*
Returns the correct dns target value based on whether it is public or private.
*/}}
{{- define "webtool.dnsTarget" -}}
  {{- if .Values.webtool.publicDns -}}
    {{ .Values.webtool.publicDnsTarget }}
  {{- else -}}
    {{ .Values.webtool.privateDnsTarget }}
  {{- end -}}
{{- end -}}