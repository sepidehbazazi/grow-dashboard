/* eslint global-require: 0 */

'use strict';

const path = require('path');
const _ = require('lodash');
const webpack = require('webpack');
const config = require('@labskit/config');

const ESlintFormatter = require('eslint-friendly-formatter');
const ESLintPlugin = require('eslint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ConfigExtractorPlugin = require('@labskit/config/config-extractor-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const VueLoaderPlugin = require('vue-loader').VueLoaderPlugin;

const nodeEnv = process.env.NODE_ENV || 'dev';

// If you want to force using compressed/uglified files in development:
// run webpack in production:"npm run webpack -- --env production" Alternatively, you can just force "isDev = false" below
// optionally disable reload in config file adding "enableWebpackRebuild : true," (Otherwise you will see annoying errors in the console)
const isDev = nodeEnv === 'dev' && !_.includes(process.argv, 'production');
const isDevServer = _.has(process.env, 'ENABLE_WEBPACK_DEV_SERVER');

const commonPlugins = [
  // Extract styles to its own file we can manually include in the html (instead of being dynamically added to the page)
  new MiniCssExtractPlugin({
    filename: isDev ? '[name].css' : '[name]-[hash].css',
    chunkFilename: isDev ? '[id].css' : '[id]-[hash].css',
  }),

  // Use the provide plugin for any libraries that need to be globally available (like jquery used to be)
  // new webpack.ProvidePlugin({
  // }),

  // Make sure momentjs doesn't load all locales and adds 900kb by itself: https://gist.github.com/xjamundx/b1c800e9282e16a6a18e#momentjs-and-the-ignore-plugin
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), // saves ~100k from build

  // This plugins maps the favicon image to the wwwroot directory
  // More info: https://github.com/webpack-contrib/copy-webpack-plugin
  new CopyWebpackPlugin({
    patterns: [{ from: './client/favicon.ico', to: 'favicon.ico' }],
  }),

  new ConfigExtractorPlugin(),
  new VueLoaderPlugin(),
  new ESLintPlugin({
    extensions: ['js', 'vue'],
    formatter: ESlintFormatter,
  }),
];

const devPlugins = [
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.LoaderOptionsPlugin({
    debug: true,
  }),
  new webpack.ProgressPlugin(),
  new FriendlyErrorsWebpackPlugin({
    compilationSuccessInfo: {
      messages: ['You application is running at http://localhost:9001'],
    },
  }),
];

// "Production" only plugins for optimization/minification, inspired by: https://blog.madewithlove.be/post/webpack-your-bags/
const productionPlugins = [
  // This plugin prevents Webpack from creating chunks that would be too small to be worth loading separately
  // TODO: Once index page is generated with webpack-html-plugin we should remove this limit and instead just add a common chunk as the vue CLI does
  new webpack.optimize.MinChunkSizePlugin({
    minChunkSize: 10240, // measured in characters, assuming 1char=1byte ~10kb
  }),
  // Vue.JS prod deployment. See: https://vuejs.org/v2/guide/deployment.html
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"',
    },
  }),
  // When building in production, css/js file names will be hashed
  // This plugin creates a json file mapping "bundle.js" and "bundle.css" with their generated names
  // We can then load it on app start, and make it available so in the main.hbs template we can include the right style/script tags
  // More info: http://madole.xyz/asset-hashing-with-webpack/
  new ManifestPlugin(),
  // Generate gzip versions of every asset, so the server does not need to generate them on the fly
  // See: https://irisschaffer.com/webpack--express--compressed-static-assets
  new CompressionPlugin(),
];

const extraPlugins = isDev ? devPlugins : productionPlugins;
const plugins = [...commonPlugins, ...extraPlugins];

if (_.includes(process.argv, '--analyze')) {
  plugins.push(
    new BundleAnalyzerPlugin({
      // See more options (like saving it as a static html page) here https://github.com/webpack-contrib/webpack-bundle-analyzer
      analyzerPort: 'auto', // see https://github.com/webpack-contrib/webpack-bundle-analyzer/issues/332
      analyzerMode: 'server',
      openAnalyzer: true,
      defaultSizes: 'parsed',
    })
  );
}

// The webpack configuration
module.exports = {
  mode: isDev ? 'development' : 'production',

  // TODO: LKT-397, switch back to source-map but using the webpack.SourceMapDevToolPlugin, excluding the vendor.js bundle from it
  devtool: isDev ? 'cheap-module-eval-source-map' : false, // for rendering sourcemaps, see https://webpack.js.org/configuration/devtool/

  // The files that sit at the root of the dependency tree and webpack starts processing to get all files to get included
  entry: {
    // this will end as the bundle.js file
    bundle: ['./client/app.js'],
    // For hot reload with webpack-dev-middleware (so when no using the webpack-dev-server), the additional endpoint below would be required:
    // .concat(isDev ? ['webpack-hot-middleware/client'] : [] )
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          // Move node_modules code to the vendor chunk, except for css/sass/scss files
          // See: https://github.com/webpack-contrib/mini-css-extract-plugin/issues/52#issuecomment-508717785
          test: /[\\/]node_modules[\\/](.(?!.*\.(s?)(c|a)ss$))*$/,
          chunks: 'initial',
          name: 'vendor',
          enforce: true,
        },
      },
    },
    minimizer: [
      new TerserPlugin({
        sourceMap: isDev,
        cache: true,
        parallel: true,
        extractComments: {
          condition: /^\**!|@preserve|@license|@cc_on/i,
          filename: 'extracted-comments.js',
          banner: licenseFile => {
            return `License information can be found in ${licenseFile}`;
          },
        },
      }),
    ],
  },

  resolve: {
    extensions: ['*', '.js', '.vue'],
    // Create aliases to import modules more easily.
    //  - Allows require('vue') instead of require('vue/dist/vue.runtime.esm.js')
    //  - Allows require('@/js/someFile') from anywhere instead of relative paths like require('../../../js/someFile')
    //  - Allows us to import config files into the client code
    alias: {
      vue$: 'vue/dist/vue.runtime.esm.js',
      '@': path.resolve('./client/js'),
      '@labskit/config': path.resolve('./config/'),
      lodash: path.resolve('./node_modules/lodash-es'), // in case anyone imports from lodash instead of lodash-es
    },
    modules: [path.resolve('./client'), 'node_modules'],
  },
  module: {
    noParse: /^(vue|vue-router|vuex|vuex-router-sync|lodash-es)$/,
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader', // Put all babel configuration in .babelrc
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader', // Will apply loaders for script/style blocks. The script uses the ".js" loaders while style will use the "sass|scss|css" loaders
      },
      {
        test: /\.json$/,
        use: 'json-loader',
      },
      {
        test: /\.(sass|scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            query: {
              importLoaders: 1,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [require('precss'), require('autoprefixer')],
              },
            },
          },
          'sass-loader',
        ],
      },
      {
        // inline fonts smaller than 10kb
        // for bigger files the file-loader is used by default, which copies file to the build folder and uses url to load it
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              outputPath: 'fonts/',
              name: '[name]-[hash].[ext]',
            },
          },
        ],
      },
      {
        // inline images less than 10kb
        // for bigger files the file-loader is used by default, which copies file to the build folder and uses url to load it
        test: /\.(png|gif|jpg|jpeg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              outputPath: 'img/',
              name: '[name]-[hash].[ext]',
              // see https://stackoverflow.com/questions/59070216/webpack-file-loader-outputs-object-module
              esModule: false,
            },
          },
        ],
      },
    ],
  },

  plugins,
  devServer: {
    contentBase: path.resolve('./client'),
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    watchOptions: {
      ignored: [
        path.resolve('./.tmp/.globalize-compiled-output/*'),
        path.resolve('./.tmp/.config-compiled-output/*'),
      ],
    },
    quiet: true, // since we use the friendly-errors-webpack-plugin
    overlay: true, // Display build errors in the browser
    lazy: false, // Set to true if you do not want the page to reload on file changes
    port: isDevServer ? config.webpack.devServer.port : 0,
  },

  output: {
    path: path.resolve(config.webpack.outputPath),
    publicPath: isDevServer ? `http://localhost:${config.webpack.devServer.port}/` : '/',
    filename: isDev ? '[name].js' : '[name]-[hash].js',
  },
};
