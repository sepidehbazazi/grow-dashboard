'use strict';

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const nodeExternals = require('webpack-node-externals');
const VueLoaderPlugin = require('vue-loader').VueLoaderPlugin;

// Recommended webpack configuration for vue-test-utils + inject-loader: https://vue-test-utils.vuejs.org/en/guides/testing-SFCs-with-mocha-webpack.html
// This is very close to our standard webpack.config.js with these changes
//    removed lots of plugins not needed during the tests
//    removed the webpack-dev-server
//    applied recommended extra vue-test-utils settings
//    updated babelrc with a test environment targetting node
module.exports = {
  // Source maps inlined for mocha-webpack
  devtool: 'inline-cheap-module-source-map',

  mode: 'development',

  // Externalize npm dependencies
  // This speeds up tests and supports test libraries that where not written with a browser in mind and cant be bundled with webpack
  externals: [
    nodeExternals({
      allowlist: [/lodash-es/],
    }),
  ],

  // Client side entry point
  entry: './client/app.js',

  resolve: {
    extensions: ['*', '.js', '.vue'],
    // Create aliases to import modules more easily.
    //  - Allows require('vue') instead of require('vue/dist/vue.common.js')
    //  - Allows require('@/js/someFile') from anywhere instead of relative paths like require('../../../js/someFile')
    alias: {
      vue$: 'vue/dist/vue.common.js',
      '@': path.resolve('./client/js'),
      '@labskit/config': path.resolve('./config/'),
      lodash: path.resolve('./node_modules/lodash-es'), // in case anyone imports from lodash instead of lodash-es
    },
    modules: [path.resolve('./client'), 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader', // Put all babel configuration in .babelrc
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader', // Will detect from .babelrc and process all <script> blocks with babel
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: {
            minimize: true,
            removeComments: true, // check additional settings in the source code: https://github.com/webpack/html-loader/blob/master/index.js#L101
          },
        },
      },
      {
        test: /\.styl$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader!stylus-loader',
            query: {
              importLoaders: 1,
            },
          },
          {
            loader: 'style-loader',
          },
        ],
      },
      {
        test: /\.scss$/,
        use: ['vue-style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            query: {
              importLoaders: 1,
            },
          },
          {
            loader: 'style-loader',
          },
        ],
      },
      {
        // inline fonts smaller than 10kb
        // for bigger files the file-loader is used by default, which copies file to the build folder and uses url to load it
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              outputPath: 'fonts/',
              name: '[name]-[hash].[ext]',
            },
          },
        ],
      },
      {
        // inline images less than 10kb
        // for bigger files the file-loader is used by default, which copies file to the build folder and uses url to load it
        test: /\.(png|gif|jpg|jpeg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              outputPath: 'img/',
              name: '[name]-[hash].[ext]',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    // Extract styles to its own file we can manually include in the html (instead of being dynamically added to the page)
    new MiniCssExtractPlugin({ filename: 'bundle.css' }),
    new VueLoaderPlugin(),
  ],

  output: {
    path: path.resolve('wwwroot'),
    publicPath: '/',
    filename: '[name].js',

    // use absolute paths in sourcemaps (important for debugging via IDE)
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]',
  },
};
