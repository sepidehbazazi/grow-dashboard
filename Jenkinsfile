// App-specific settings //

env.DOCKER_IMAGE_NAME = "grow-app"

// BRANCH_NAME will be the git branch ("master", "test", etc), or the PR ("PR-1", "PR-123")
env.STACK_NAME = "grow-app-${BRANCH_NAME.toLowerCase()}"

BRANCHES_TO_DEPLOY = [
    // eg. [branch: "master", port: 3001]  // Use the default dns name
    // eg. [branch: "master", port: 3001, dns: "some.fancy.url.com"]  // Use explicit/public dns name

    // Example: all branches have explicit ports & names, but the PR has automatic dns name.
    // [branch: "master", port: 3001, dns: "test-app-latestcommit.owlabs.io"],
    // [branch: "test", port: 3002, dns: "test-app-test.owlabs.io"],
    // [branch: "staging", port: 3003, dns: "test-app-staging.owlabs.io"],
    // [branch: "production", port: 3004, dns: "test-app.owlabs.io"],
    // [branch: "PR-1", port: 3005],

    // Just build master, automatic DNS name
    [branch: "master", port: 8050]
]

////////////////////////////////////////////////////////////////////////////////

node {
    assert CHEFENV
    assert SITE_CODE

    env.ESCAPED_BUILD_TAG = BUILD_TAG.replaceAll(" ", "-")
    env.REGISTRY_URL = "docker-registry.${CHEFENV}.${SITE_CODE}.owlabs.io"

    properties([pipelineTriggers([[$class: 'PeriodicFolderTrigger', interval: '5m']])])

    stage("prep") {
        checkout scm
    }

    stage("build") {
        docker.build("${DOCKER_IMAGE_NAME}:${ESCAPED_BUILD_TAG}", " -f ./Dockerfile .")
    }

    stage("package") {
        withCredentials([
            usernamePassword(credentialsId: 'docker-registry', passwordVariable: 'REGISTRY_PASS', usernameVariable: 'REGISTRY_USER')
        ]) {
            sh "docker login -u \"${REGISTRY_USER}\" -p \"${REGISTRY_PASS}\" ${REGISTRY_URL}"
            sh "docker tag ${DOCKER_IMAGE_NAME}:${ESCAPED_BUILD_TAG} ${REGISTRY_URL}/${DOCKER_IMAGE_NAME}:${ESCAPED_BUILD_TAG}"
            sh "docker push ${REGISTRY_URL}/${DOCKER_IMAGE_NAME}:${ESCAPED_BUILD_TAG}"
        }
    }

//    stage("scan") {
//        withCredentials([
//            usernamePassword(credentialsId: 'docker-registry', passwordVariable: 'REGISTRY_PASS', usernameVariable: 'REGISTRY_USER')
//        ]) {
//            // REQUIRED: This scans for vulnerabilities.  Do not remove.
//            // Must pull first, since "latest" may already exist but be out of date.
//            // The --group-add is necessary here, otherwise the container user "clairctl" does not have access to the docker.sock.  So we map in the host's "docker" group.
//            // TODO: Must use CLAIR_PORT=0 here, or else clairctl sends the port in the "Host" header which fails TLS/haproxy.
//            sh "docker pull ${REGISTRY_URL}/owg/labs-docker-clairctl:latest"
//            sh "docker run --rm \
//                -e CLAIR_PORT=0 \
//                -e CLAIR_URL=https://clair.default.${CHEFENV}.${SITE_CODE}.owlabs.io \
//                -e REGISTRY_PASSWORD=${REGISTRY_PASS} \
//                -e REGISTRY_URL=${REGISTRY_URL} \
//                -e REGISTRY_USERNAME=${REGISTRY_USER} \
//                -v /var/run/docker.sock:/var/run/docker.sock:ro \
//                --group-add \$(getent group docker | cut -d: -f3) \
//                ${REGISTRY_URL}/owg/labs-docker-clairctl:latest ./analyze.sh ${REGISTRY_URL}/${DOCKER_IMAGE_NAME}:${ESCAPED_BUILD_TAG}"
//        }
//    }
    
    stage("deploy") {
        // TODO: Can't use .find() or .findAll() here due to jenkins groovy script-security sandbox.
        // ERROR: org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException: Scripts not permitted to use staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods find java.util.Collection groovy.lang.Closure
        // This is because the jenkins "script security" plugin is old (v1.27), the latest version allows this.
        // deploymentInfo = BRANCHES_TO_DEPLOY.find { it.branch == BRANCH_NAME }
        // WHITELIST: https://github.com/jenkinsci/script-security-plugin/blob/script-security-1.27/src/main/resources/org/jenkinsci/plugins/scriptsecurity/sandbox/whitelists/generic-whitelist
        deploymentInfo = null
        for (item in BRANCHES_TO_DEPLOY) {
          if (item.branch == BRANCH_NAME) {
            deploymentInfo = item
            break
          }
        }

        if (RANCHER_URL == null) {
            echo "RANCHER_URL not specified, cannot deploy"
        } else if (deploymentInfo) {

            // PORT is required (for now)
            env.PORT = deploymentInfo.port
            // DNS_NAME is optional
            env.DNS_NAME = deploymentInfo.dns ?: ""

            echo "Deploying branch \"${BRANCH_NAME}\", port \"${PORT}\", dns name \"${DNS_NAME}\""
            withCredentials([
                usernamePassword(credentialsId: 'rancher', passwordVariable: 'RANCHER_PASS', usernameVariable:'RANCHER_USER')
            ]) {
                sh "rancher --environment ${RANCHER_ENV} --url ${RANCHER_URL} --access-key ${RANCHER_USER} --secret-key \"${RANCHER_PASS}\" -- up -d --force-upgrade --pull --stack ${STACK_NAME} --confirm-upgrade --file docker-compose-rancher.yml"
            }
        } else {
            echo "Not deploying branch \"${BRANCH_NAME}\""
        }
    }
}
