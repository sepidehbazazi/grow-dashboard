// ===============================================================================================================
// Build Jenkinsfile.
// This file defines the pipeline that builds and test new docker images for your projects
//   - If successful, publishes the new image in nexus as the "latest" docker image of your project
//   - When building the master branch, the deploy job is automatically triggered with environment=latestcommit
// Use the deploy job to deploy the published images to any environment (like latestcommit, stage or prod)
// ===============================================================================================================
node('docker') {
  slackSend color: '#D4DADF', message: "Build Started - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"

  try {
    def BITBUCKET_REPO = "grow-dashbaord"
    def BITBUCKET_REPO_URL = "https://bitbucket.org/sepidehbazazi/${BITBUCKET_REPO}"
    def BITBUCKET_PROJECT_CODE = "dmo"
    def SONARQUBE_URL = env.SONARQUBE_URL ?: "https://sonarqube.${env.SITE_CODE}.owlabs.io/"
    def SONARQUBE_KEY = env.SONARQUBE_KEY ?: BITBUCKET_REPO
    def NEXUS_FOLDER = BITBUCKET_PROJECT_CODE // This is the recommended convention. Update if you used a different content selector when setting up the Nexus docker registry as per: https://owlabs.atlassian.net/wiki/spaces/CHIPS/pages/755171523/Setup+Nexus+for+a+new+Rancher+project
    def registry = "docker-registry.${env.SITE_CODE}.owlabs.io"
    def prodImageName = "${registry}/${NEXUS_FOLDER}/${BITBUCKET_REPO}"
    def buildingReleaseTag = env.TAG_NAME && !!(env.TAG_NAME =~ /release-.*/)
    def willPackageAndDeploy = env.BRANCH_NAME == "master" || buildingReleaseTag
    def gitCommit = ''
    // expose back image names as env variables we can reference in compose file
    env.IMAGE_NAME = prodImageName

    stage("checkout") {
      checkout scm
      gitCommit = sh(returnStdout: true, script: "git rev-parse HEAD").trim()
    }

    if (buildingReleaseTag) {
      stage("scan deploy job"){
        // when we need to trigger the deploy job, this ensures we have scanned the deploy multibranch pipeline
        // and the branches/tags are discovered without relying exclusively on webhooks
        build(job: "../${BITBUCKET_REPO}-deploy", wait: false)
      }
    }

    stage("build image") {
      sh """
        docker image build \
          -f ./Dockerfile \
          -t ${prodImageName} .
      """
    }

    stage("dependencies scan") {
      // Scan the build prod container using trivy
      // This includes both OS packages inside the container and libraries like npm/pip dependencies
      //    https://github.com/aquasecurity/trivy
      //    https://owlabs.atlassian.net/wiki/spaces/CHIPS/pages/1522827650/Trivy
      sh """
        docker run --rm \
          -v /var/run/docker.sock:/var/run/docker.sock \
          -v \$HOME/Library/Caches:/root/.cache/ \
          aquasec/trivy -q image \
          --severity CRITICAL,HIGH,MEDIUM,LOW \
          ${prodImageName}
      """
      // Command above will print results to the jenkins console
      // Now fail the build if critical/high vulnerabilities are found (repeating the test is really unexpensive)
      sh """
        docker run --rm \
          -v /var/run/docker.sock:/var/run/docker.sock \
          -v \$HOME/Library/Caches:/root/.cache/ \
          aquasec/trivy -q image \
          --exit-code 1 --severity CRITICAL,HIGH \
          ${prodImageName}
      """
    }

    if (willPackageAndDeploy) {
      stage("sonarqubeScan") {
        withCredentials([
          string(credentialsId: 'sonarqube', variable: 'sonarqubeApiToken')]
        ) {
          if (!sonarqubeApiToken) {
            echo "Sonarqube token not provided. Create a token and update your jenkins chart"
            return
          }
          def versionTag = env.BRANCH_NAME == "master" ? "master" : env.TAG_NAME
          sh """
            docker run --rm\
              -v \$HOME:/opt/sonar-scanner/.sonar/cache \
              -v \$(pwd):/usr/src \
              -e SONAR_HOST_URL="${SONARQUBE_URL}" \
              sonarsource/sonar-scanner-cli \
                -Dsonar.projectKey=${SONARQUBE_KEY} \
                -Dsonar.login=${sonarqubeApiToken} \
                -Dsonar.projectVersion=${versionTag}
          """
        }
      }

      stage('package') {
        // by default use the image name without tag (so it is published to the registry as "latest")
        def taggedImageName = "${prodImageName}:latest"
        // if building a specific git tag, then use it also as the docker image tag
        if (buildingReleaseTag) {
          taggedImageName = "${prodImageName}:${env.TAG_NAME}"
          sh "docker tag ${prodImageName} ${taggedImageName}"
        }
        // finally publish the image using either the latest or tagged image tag
        withCredentials([
          usernamePassword(credentialsId: 'registry',  usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')
        ]) {
          sh """
            docker login -u '${USERNAME}' -p '${PASSWORD}' ${registry}
            docker push ${taggedImageName}
          """
        }
      }

      stage("deploy") {
        // Trigger the deploy job (which should be at the same folder than the build job, we need the ".." to get out of the current branch)
        // Since it is a Jenkins multibranch pipeline, we need to specify which branch/tag we want to run
        build job: "../${BITBUCKET_REPO}-deploy/${buildingReleaseTag ? env.TAG_NAME : 'master'}",
          wait: false,
          parameters: [
            string(name: 'environment', value: "latestcommit"),
          ]
      }
    }

    slackSend color: 'good', message: "Build Successful - ${env.JOB_NAME} ${env.BUILD_NUMBER}"
  } catch (exc) {
    slackSend color: 'danger', message: "Build Failed - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
    throw exc
  }
}
