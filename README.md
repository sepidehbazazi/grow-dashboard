# Grow Dashboard

This project is designed to build a clickable dashboard capabale of showing 
OW carbon emissions data to users.

## Setup

Currently the application is build on a Python foundation.

To get started, we recommend building a new Python virtual environment, and installing all of the
requirements in the `requirements.txt` file.

To build a new virtualenvironment, run `python3 -m venv venv`.

To activate the environment run `source venv/bin/activate` or `source venv/Scripts/activate`
depending on Mac or Windows respectively.

Install the requirements: `pip install -r requirements.txt`

To run the code, you will need to 'activate' the virtual environment for every session.

## Running

### Data 
Summary .csv files containing the necessary emissions data are created and stored in this repo.
These can be found in data/ . 

### Dashboard
To start the dashboard, use `python grow_dashboard.py`.
It takes no arguments and will start a dashboard running against localhost
In the present state, the dashboard will use pre-gathered and processed data files in the /data folder (created manually).

#### Docker

The dashboard can also be run via docker. To do so, you will need to run two commands:

1. `$ ./docker_build.sh` : this will build the docker container
2. `$ docker run -p 8050:8050 grow-app` This will turn on the container and bind the service to port 8050

After that you should be able to go to [localhost:8050](localhost:8050) to see the dashboard.

