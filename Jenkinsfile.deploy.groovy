// ===============================================================================================================
// Deploy Jenkinsfile.
// This file defines the pipeline that deploys images from nexus into one of your project environments
//   - The image tag name are inferred from the branch/tag being built:
//        if building master branch, the "latest" image is used
//        otherwise, an image with the same tag than the current branch/tag being built is used
//   - The images should have been published before by the build job.
//     The build job will always push the "latest" image tag. In addition, bitbucket tags following the "release-*" format will be also build and published
//   - The "latest" image tag can only be used when deploying to latestcommit
//   - The Kubernetes namespace into which containers will be deployed needs to be specified, and needs to exist in Kubernetes
// ===============================================================================================================
properties([
  parameters([
    choice(
      name: 'environment',
      description: 'Which environment to deploy to.',
      choices: ['latestcommit', 'staging', 'production']
    )
  ])
])
node('docker') {
  slackSend color: '#D4DADF', message: "Build Started - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"

  try {
    def NAMESPACE_ROOT = "grow-dashboard" // This and the environment parameter combined form the namespace
    def BITBUCKET_REPO = "grow-dashboard"
    def BITBUCKET_PROJECT_CODE = "dmo"
    def NEXUS_FOLDER = BITBUCKET_PROJECT_CODE // This is the recommended convention. Update if you used a different content selector when setting up the Nexus docker registry as per: https://owlabs.atlassian.net/wiki/spaces/CHIPS/pages/755171523/Setup+Nexus+for+a+new+Rancher+project

    def registry = "docker-registry.${env.SITE_CODE}.owlabs.io"
    def imageName = "${registry}/${NEXUS_FOLDER}/${BITBUCKET_REPO}"
    def tagName = env.TAG_NAME ?: "latest"
    def taggedImageName = "${imageName}:${tagName}"
    def namespace = "${NAMESPACE_ROOT}-${params.environment}"

    def overrideFile = "./build/chart/environment-overwrites/${params.environment}.yaml";
    def gitCommit = ''

    stage('checkout'){
      checkout scm
    }

    // Enforce tagging rules
    //  - specified image should exist, so the build job should have build and published first to the registry
    //  - when deploying to staging/production, only tagged images can be used (ie, images other than latest)
    stage('Verify docker registry tag'){
      withCredentials([
        usernamePassword(credentialsId: 'registry', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')
      ]) {
        sh "docker login -u '${USERNAME}' -p '${PASSWORD}' ${registry}"

        if (tagName == "latest" && params.environment != "latestcommit") {
          error "The latest image can only be deployed to latestcommit!"
        }
        try {
          sh "docker pull ${taggedImageName}"
          gitCommit = sh(returnStdout: true, script: "docker run --rm --entrypoint='' ${taggedImageName} sh -c 'echo \$GIT_COMMIT'").trim()
        } catch(e) {
          error "Could not find image ${tagName}. Was the image built and published by the build job?"
        }
      }
    }

    stage('lint chart') {
      sh """
        helm lint ./build/chart \
          --values  ${overrideFile} \
          --set clusterName=${env.RANCHER_CLUSTER_NAME} \
          --set regionName=${env.SITE_CODE} \
          --set webtool.image=${taggedImageName} \
          --set webtool.versionTag=${tagName} \
          --set webtool.commit=${gitCommit} \
          --set webtool.publicDnsTarget=${env.DNS_PUBLIC_TARGET} \
          --set webtool.privateDnsTarget=${env.DNS_PRIVATE_TARGET} \
      """
    }

    stage('deploy') {
      withCredentials([
        usernamePassword(credentialsId: 'rancher',  usernameVariable:'RANCHER_URL', passwordVariable: 'RANCHER_TOKEN')
      ]) {
        sh "rancher login ${RANCHER_URL} --token ${RANCHER_TOKEN} --context ${env.RANCHER_PROJECT_ID} --skip-verify"

        // Does the namespace exist? If it doesnt, we need to create the namespace as part of the right rancher project
        // For that we can list current namespaces with "rancher namespace -q" and search for the one we want
        // NOTE: grep command returns 0 if results found, 1 if no results found, 2 if error
        def namespaceExists = sh(returnStatus: true, script: "rancher namespace -q | grep ${namespace}\$") == 0
        if (!namespaceExists) {
          sh "rancher namespace create ${namespace}"
        }

        // Get the URL that we can use for kubectl/helm to connect to the specific cluster
        // RANCHER_PROJECT_ID comes in the form "c-htzn7:p-tqh7h", where c-htzn7 is the cluster and p-tqh7h the project
        def clusterId = env.RANCHER_PROJECT_ID.tokenize(':')[0]
        def clusterURL = "${RANCHER_URL}k8s/clusters/${clusterId}"

        // Add the build number to the chart version we are deploying, so every deployment has its own unique version.
        // This way commands like `helm history` will be more useful.
        //    note in macs the command would be: sed -E -i '' -e 's/version: .*\$/&.${env.BUILD_NUMBER}/' ./build/chart/Chart.yaml
        sh "sed -E -i 's/version: .*\$/&.${env.BUILD_NUMBER}/' build/chart/Chart.yaml"

        // We use the namespace also as the name of the "helm release"
        def helmReleaseName = namespace

        // Install the chart with Helmv3
        //  - the updgrade command with "--install" will install the chart if it wasnt previously installed
        //  - the "--wait" and "--timeout" parameters mean it will wait for 5m until all resources are created and _healthy_, otherwise it fails
        //  - kube-apiserver points to the rancher URL, with the path of the cluster we want
        //  - kube-token is the same as the rancher token, to grant access to that cluster
        // Once installed, you should see your chart and its version with "helm list --all-namespaces"
        // For more information, see https://helm.sh/docs/helm/helm_upgrade/
        sh """
          helm upgrade \
            ${helmReleaseName} ./build/chart \
            --install \
            --kube-apiserver ${clusterURL} \
            --kube-token ${RANCHER_TOKEN} \
            --namespace ${namespace} \
            --values  ${overrideFile} \
            --set clusterName=${env.RANCHER_CLUSTER_NAME} \
            --set regionName=${env.SITE_CODE} \
            --set webtool.image=${taggedImageName} \
            --set webtool.versionTag=${tagName} \
            --set webtool.commit=${gitCommit} \
            --set webtool.publicDnsTarget=${env.DNS_PUBLIC_TARGET} \
            --set webtool.privateDnsTarget=${env.DNS_PRIVATE_TARGET} \
            --wait --timeout 5m0s
        """
      }
    }

    slackSend color: 'good', message: "Build Successful - ${env.JOB_NAME} ${env.BUILD_NUMBER}"
  } catch (exc) {
    slackSend color: 'danger', message: "Build Failed - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
    throw exc
  }
}
